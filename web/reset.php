<?php
	require_once("action/ResetAction.php");

	$action = new ResetAction();
	$action->execute();
	$token = $action->token;
	$mdpChanger = $action->mdpChanger;
	$mdpChangerFail = $action->mdpChangerFail;
	require_once("partial/header.php");
?>
<div class="conteneur">
<div class="container">
<h1>Entrez votre courriel et votre nouveau mot de passe</h1>
<div>
			<?php 
					if ($mdpChangerFail == true) {
						?>
						<div class="alert alert-danger" role="alert">ERREUR ~ Cliquer sur le lien envoyer par courriel pour changer votre mot de passe</div>
						<?php
					}
				?>
		</div>
		<div>
			<?php 
					if ($mdpChanger == true) {
						?>
						<div class="alert alert-success" role="alert">Votre mot de passe a été changer, connectez vous !</div>
						<?php
					}
				?>
		</div>
<form  method="post" class="form-signin">
			<div class="form-group col-md-4">
				</div>

					<div class="form-group col-md-12">
						<input id="AreaUsername" name="email" class="form-control" placeholder="Adresse courriel" required autofocus>
					</div>
					<div class="form-group col-md-12">
						<input type="password" id="AreaPassword" name="newPassword" class="form-control" placeholder="nouveau mot de passe" required autofocus>
					</div>
				<div class="form-group col-md-12">
					<button class="btn btn-md btn-primary" type="submit">Changez votre mot de passe</button>
				</div>
</form>
</div>
</div>
<?php
// include  FOOTER
  require_once("partial/footer.php");