<?php
	require_once("action/ModifierProfilAction.php");

	$action = new ModifierProfilAction();
	$action->execute();
	$validationPassword =true;
	$newPassword =false;
	if ( isset($_POST["currentPassword"])) {
		$action->saveChanges();
		$validationPassword = $action->validationPassword;
		$newPassword = $action->newPassword;
	}

	$user = $action->user;

  require_once("partial/header.php");
?>
<div class="conteneur">
<div class="conteneur">
		<div class="container">
		<div>
			<?php 
					if ($validationPassword == false) {
						?>
						<div class="alert alert-danger" role="alert"><strong>Réessayer ~</strong> Mauvais mot de passe</div>
						<?php
					}
				?>
		</div>
		<div>
			<?php 
					if ($newPassword == true) {
						?>
						<div class="alert alert-success" role="alert"><strong>Bravo ~</strong> Votre profil a été modifié, rafraichissez la page pour voir les changements</div>
						<?php
					}
				?>
		</div>
<div class="container">
        <form class="form-horizontal" action="modifierProfil" role="form" method="post">
          <div class="form-group">
            <label class="col-lg-3 control-label">First name:</label>
            <div class="col-lg-8">
              <input class="form-control" type="text" name="newFirstName" value="<?= htmlspecialchars($user['PRENOM'], ENT_QUOTES, 'UTF-8') ?>">
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 control-label">Last name:</label>
            <div class="col-lg-8">
              <input class="form-control" type="text"  name="newLastName" value="<?= htmlspecialchars($user['NOM'], ENT_QUOTES, 'UTF-8')  ?>">
            </div>
          </div>

          <div class="form-group">
            <label class="col-lg-3 control-label">Email:</label>
            <div class="col-lg-8">
              <input class="form-control" type="text" name="newEmail" value="<?=  htmlspecialchars($user['COURRIEL'] , ENT_QUOTES, 'UTF-8')?>">
            </div>
          </div>
         
          <div class="form-group">
            <label class="col-lg-3 control-label">Username:</label>
            <div class="col-lg-8">
              <input class="form-control" type="text" name="newUsername" value="<?=   htmlspecialchars($user['USERNAME'], ENT_QUOTES, 'UTF-8')?>">
            </div>
          </div>

		  <div class="form-group col-lg-13 colorPickerContainer2">
				<label for="tankColorHex">Couleur du tank</label>

				<?php require_once("partial/colorPicker.php")?>
				<input type="text" id="tankColorHex" name="tankColorHex" value="<?=  htmlspecialchars($user['COULEUR_TANK'] , ENT_QUOTES, 'UTF-8')?>" readonly>

			</div>
         
          <div class="form-group">
            <label class="col-lg-3 control-label">New password:</label>
            <div class="col-lg-8">
              <input class="form-control" type="password" name="newPassword">
            </div>
          </div>

		  <div class="form-group">
            <label class="col-lg-3 control-label">* Current Password:</label>
            <div class="col-lg-8">
              <input class="form-control" type="password" name="currentPassword" value="" required>
            </div>
          </div>

          <div class="form-group">
            <label class="col-lg-3 control-label"></label>
            <div class="col-lg-8">
              <input type="submit" class="btn btn-primary" value="Save Changes">
              <span></span>
              <input type="reset" class="btn btn-default" value="Cancel">
            </div>
          </div>
        </form>
      </div>
  </div>
</div>
</div>