<?php
// include  CONTROLER
// include  HEADER
  require_once("action/IndexAction.php");
  
  $action = new IndexAction();
  $action->execute();

  require_once("partial/header.php");
  require_once("partial/publicProfileTemplate.php");
?>
<script src="js/indexUIcontroler.js"></script>
<!-- Body -->
<div id="globalContainer">
  <div id="pagerCTL">
    <button id="prevBtn" class="btn btn-primary" style="color:white;padding-left:30px;padding-right:30px;">Previous</button>
    <p id="pageNumber"></p>
    <button id="nextBtn" class="btn btn-primary" style="color:white;padding-left:30px;padding-right:30px;">Next</button>
  </div>
  <div id="tankemTitle">
    <img src="images/titre.png" alt="tankemTitre">
  </div>
  <div id="allProfileContainer">
  </div>
</div>

<?php
// include  FOOTER
  require_once("partial/footer.php");
  echo $action->result;