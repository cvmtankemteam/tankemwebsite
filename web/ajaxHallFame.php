<?php
    require_once("action/HallFameAction.php");

    $action = new HallFameAction();
    $action->execute();
    
    $hallFame = $action->listTopUsers;
    $result = array();
    
    foreach($hallFame as $player){
        $result[] = $player["ID"];
        if(sizeof($result) == 10){
            break;
        }
    }

    echo json_encode($result);