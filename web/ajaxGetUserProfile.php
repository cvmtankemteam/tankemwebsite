<?php
    require_once("action/AjaxGetUserProfileAction.php");

    $action = new AjaxGetUserProfileAction();
    $action->execute();

    echo json_encode($action->result);