<?php
    require_once("action/AjaxLastFiveAction.php");

    $action = new AjaxLastFiveAction();
	$action->execute();
    
    echo json_encode($action->result);