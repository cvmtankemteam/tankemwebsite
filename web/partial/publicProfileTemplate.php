<script type="x-template" id="publicProfileTemplate">

<section>
    <span>
        <p class="level">99</p>
    </span>
    <span>
        <p class="nomCalcule">Wolflandre crossfiter qui fait du crossfit</p>
        <p class="hallFameInfo">Hall of Fame</p>
    </span>
    <div class="badgeZone">
        <div class="hallBadge">
            <img src="images/hallfameBadges/sergentMajorOfArmy.png">
        </div>
    </div>
</section>
<section>
    <div>
        <img class="tank" src="images/tankProfile.png" alt="Image du tank du joueur">
    </div>

    <div>
        <h4>Statistique</h4>
        <span class="statLabel">Nombre de partie Jouees </span>
        <span class="statValue partieJouee">10</span>
        
        <span class="statLabel">Nombre de partie gagnees </span>
        <span class="statValue partieGagne">5</span>
        
        <span class="statLabel">Ratio de victoire </span>
        <span class="statValue ratioVictoire">50%</span>

        <h4>Carte Preferees</h4>
        <span class="statLabel carte"></span>

        <h4>Armes Preferees</h4>
        <div class="statLabel arme1">Canon</div>
        <div class="statLabel arme2">Shutgun</div>
    </div>
</section>
</script>

<script type="x-template" id="rankingImg">

    <img src="images/hallfameBadges/sergentMajorOfArmy.png" alt="Image Ranking">
    <img src="images/hallfameBadges/sergentCommandant.png" alt="Image Ranking">
    <img src="images/hallfameBadges/sergentMajor.png" alt="Image Ranking">
    <img src="images/hallfameBadges/premierSergent.png" alt="Image Ranking">
    <img src="images/hallfameBadges/masterSergent.png" alt="Image Ranking">
    <img src="images/hallfameBadges/sergentFirstClass.png" alt="Image Ranking">
    <img src="images/hallfameBadges/stafSergent.png" alt="Image Ranking">
    <img src="images/hallfameBadges/sergent.png" alt="Image Ranking">
    <img src="images/hallfameBadges/corporal.png" alt="Image Ranking">
    <img src="images/hallfameBadges/firstClass.png" alt="Image Ranking">
    <img src="images/hallfameBadges/private.png" alt="Image Ranking">

</script>
