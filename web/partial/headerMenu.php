<script src="js/navHeader.js"></script>
<nav id="navHeader">
    <h1>Tankem</h1>
    <ul>
        <li><a href="index">Profiles</a></li>
        <li><a href="lastfive">Last Five</a></li>
        <li><a href="hallfame">Hall of Fame</a></li>
        <li><a href='replay'>Replay</a></li>
        <?php
            if(isset($_SESSION["visibility"]) && $_SESSION["visibility"] > 0){
                echo "<li><a href='attribut'>Attribut du tank</a></li>";
                echo "<li><a href='modifierProfil'>Modifier le profil</a></li>";
                echo "<li><a href='logout'>Logout</a></li>";
            }
            else{
                echo "<li><a href='login'>Login/S'enreristrer</a></li>";
            }
        ?>
    </ul>
    <input id="navHeaderInput" type="text" placeholder="search">
    <button id="navHeaderBtn" type="button" class="btn btn-primary" style="color:white;padding-left:30px;padding-right:30px;">Search</button>
</nav>