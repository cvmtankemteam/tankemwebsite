<?php
// include  CONTROLER
// include  HEADER
  require_once("action/AttributAction.php");
  
  $action = new AttributAction();
  $action->execute();

  require_once("partial/header.php");
?>
<script src="js/attribut.js"></script>
<!-- Body -->
<div class="container">
<h2>Statistiques du tank</h2>
<table class="table table-striped">
    <thead>
        <th class="text-center">Vie</th>
        <th class="text-center">Force</th>
        <th class="text-center">Dextérité</th>
        <th class="text-center">Agilité</th>
    </thead>
    <tr>
        <td class="text-center"> <?= $action->stats_vie ?> </td>
        <td class="text-center"> <?= $action->stats_force ?> </td>
        <td class="text-center"> <?= $action->stats_dex ?> </td>
        <td class="text-center"> <?= $action->stats_agil ?> </td>
    </tr>
</table>

<form action="attribut.php" method="post">
	<h2>Modification d'attributs</h2>
	<div class="col-lg-3">
		<div class="attributs-table">
            <div class="trigger form-group form-group-lg form-group-icon-left"><i class="fa fa-users input-icon input-icon-highlight"></i>
                <label>Point(s) en banque</label>
                <input type="text" name="attributs" id="attributs" class="form-control" value="<?= $action->point_attribut ?>" readonly>
            </div>
            <!-- Vie -->
            <div class="form-group">
                    <label class="control-label col-md-5"><strong>Vie</strong><br> <i>Endurance du tank</i></label>
                    <div class="input-group number-spinner col-md-7">
                    <span class="input-group-btn">
                        <a class="btn btn-danger" data-dir="dwn"><span class="glyphicon glyphicon-minus"></span></a>
                    </span>
                    <input type="text" readonly name="vie" id="vie" class="form-control text-center" value=" <?= $action->point_vie ?> " max="30" min="<?= $action->point_vie ?>" >
                    <span class="input-group-btn">
                        <a class="btn btn-info" data-dir="up"><span class="glyphicon glyphicon-plus"></span></a>
                    </span>
                </div>
            </div>
            <!-- Force -->
            <div class="form-group">
                <label class="control-label col-md-5"><strong>Force</strong><br> <i>Puissance de tir</i></label>
                <div class="input-group number-spinner col-md-7">
                    <span class="input-group-btn">
                        <a class="btn btn-danger" data-dir="dwn"><span class="glyphicon glyphicon-minus"></span></a>
                    </span>
                    <input type="text" readonly name="force" id="force" class="form-control text-center" value=" <?= $action->point_force ?> " max="30" min="<?= $action->point_force ?>" >
                    <span class="input-group-btn">
                        <a class="btn btn-info" data-dir="up"><span class="glyphicon glyphicon-plus"></span></a>
                    </span>
                </div>
            </div>
            <!-- Dextérité -->
            <div class="form-group">
                <label class="control-label col-md-5"><strong>Dextérité</strong><br> <i>Vitesse de rotation</i></label>
                <div class="input-group number-spinner col-md-7">
                    <span class="input-group-btn">
                        <a class="btn btn-danger" data-dir="dwn"><span class="glyphicon glyphicon-minus"></span></a>
                    </span>
                    <input type="text" readonly name="dex" id="dex" class="form-control text-center" value=" <?= $action->point_dex ?> " max="30" min="<?= $action->point_dex ?>" >
                    <span class="input-group-btn">
                        <a class="btn btn-info" data-dir="up"><span class="glyphicon glyphicon-plus"></span></a>
                    </span>
                </div>
            </div>
            <!-- Agilité -->
            <div class="form-group">
                <label class="control-label col-md-5"><strong>Agilité</strong><br> <i>Accélération</i></label>
                <div class="input-group number-spinner col-md-7">
                    <span class="input-group-btn">
                        <a class="btn btn-danger" data-dir="dwn"><span class="glyphicon glyphicon-minus"></span></a>
                    </span>
                    <input type="text" readonly name="agil" id="agil" class="form-control text-center" value=" <?= $action->point_agil ?> " max="30" min="<?= $action->point_agil ?>">
                    <span class="input-group-btn">
                        <a class="btn btn-info" data-dir="up"><span class="glyphicon glyphicon-plus"></span></a>
                    </span>
                </div>
            </div>
            <button class="btn btn-default btn-block demise" type="submit">Accepter</a>
        </div>
	</div>
</form>
</div>
<?php
// include  FOOTER
  require_once("partial/footer.php");