<?php
// include  CONTROLER
// include  HEADER
  require_once("action/HallFameAction.php");
  
  $action = new HallFameAction();
  $action->execute();

  require_once("partial/header.php");

?>
<!--<script src="js/attribut.js"></script>-->
<!-- Body -->
<div class="container">
<h2>Hall of Fame</h2>
  <p>Liste des dix meilleurs joueurs</p>            
  <table class="table table-striped">
    <thead>
      <tr>
        <th class="text-center">Position</th>
        <th class="text-center">Rang</th>
        <th class="text-center">Username</th>
        <th class="text-center">Nom calculé</th>
        <th class="text-center">Niveau du joueur</th>
        <th class="text-center">Map préférée</th>
        <th class="text-center">Victoire/Défaite</th>
        <th class="text-center">Nombre de parties jouées</th>
        <th class="text-center">Tank</th>
      </tr>
    </thead>
    <tbody>
    <?php
    $count = 0;
    foreach ($action->listTopUsers as $topUser) {
        
        ?>
    <tr>
        <td class="text-center"> #<?= $count+1 ?> </td>
        <td class="text-center"> <?= $action->logoArray[$count] ?> </td>
        <td class="text-center"> <?= $topUser["USERNAME"] ?> </td>
        <td class="text-center"> <?= $topUser["QUALIFICATIF_A"] . " " . $topUser["QUALIFICATIF_B"] ?> </td>
        <td class="text-center"> <?= $topUser["NIVEAU"] ?> </td>
        <td class="text-center"> <?= $topUser["FAVORITE_MAP"] ?> </td>
        <td class="text-center"> <?= $topUser["WIN_RATE"] ?> </td>
        <td class="text-center"> <?= $topUser["GAME_PLAYED"] ?> </td>
        <?php 
        if ( ($count+1)%2 == 1 )
        {
        ?>
        <td class="text-center"> <img class="tank" src="images/tankProfile.png" alt="Image du tank du joueur" width="169" height="117" style="background-color:<?= $topUser["COULEUR_TANK"] ?>;" > </td>
        <?php 
        }
        else {
        ?>
        <td class="text-center"> <img class="tank" src="images/tankProfileSand.png" alt="Image du tank du joueur" width="169" height="117" style="background-color:<?= $topUser["COULEUR_TANK"] ?>;" > </td>
        <?php 
        }
        ?>
    </tr>

      <?php
      $count += 1;
    }
      ?>
    </tbody>
  </table>
</div>