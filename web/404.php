<?php
	require_once("action/Page404Action.php");

	$action = new Page404Action();
	$action->execute();

  require_once("partial/header.php");
?>

<div class="conteneur">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="error-template">
					<h1>
						Oops!</h1>
					<h2>
						ERREUR 404</h2>
					<div class="error-details">
						La page recherché n'a pas été trouvé, a moins que tu ne cherche à briser le site d'une quelconque façon, fit toi a notre beau menu pour te retrouver !
					</div>
					<div class="error-actions">
						<a href="index" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-home"></span>
							Retourner à l'index </a>
					</div>
					<img src="images/404.gif" alt="pagenotFound">
				</div>
			</div>
		</div>
    </div>
</div>


<?php
// include  FOOTER
  require_once("partial/footer.php");
