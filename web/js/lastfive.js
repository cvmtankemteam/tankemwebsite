$(function() {

getLastFive();

})

function getLastFive() {
     $.ajax({
        url: "ajax-last-five.php",
        type: "POST" ,
        data: {}
    })
    .done(function(data) {
        let donnees = JSON.parse(data);

        let couleurFond = 0;

        let mainNode = document.getElementById("info");
        mainNode.innerHTML = "";

        donnees.forEach(function(element) {
            couleurFond += 1;
            let gameNode = document.createElement("tr");
            let nodeWinner = document.createElement("td");
            nodeWinner.setAttribute("class","text-center");
            let nodeTankWinner = document.createElement("td");
            nodeTankWinner.setAttribute("class","text-center");
            let nodeLoser = document.createElement("td");
            nodeLoser.setAttribute("class","text-center");
            let nodeTankLoser = document.createElement("td");
            nodeTankLoser.setAttribute("class","text-center");
            let nodeCarte = document.createElement("td");
            nodeCarte.setAttribute("class","text-center");
            nodeCarte.appendChild( document.createTextNode( element["NOM_NIVEAU"] ) );

            element["PLAYERS"].forEach(function(player) {
                
                let nodeUsername = document.createElement("div");
                nodeUsername.appendChild( document.createTextNode(player["USERNAME"]) );
                let nodeNiveau = document.createElement("div");
                nodeNiveau.appendChild( document.createTextNode("Level : " + player["NIVEAU"]) );
                let nodeNomGenere = document.createElement("div");
                if (player["QUALIFICATIF_A"] == null) {
                    player["QUALIFICATIF_A"] = "";
                }
                if (player["QUALIFICATIF_B"] == null) {
                    player["QUALIFICATIF_B"] = "";
                }
                nodeNomGenere.appendChild( document.createTextNode(player["QUALIFICATIF_A"] + " " + player["QUALIFICATIF_B"]) );
                let nodeImageTank = document.createElement("img");
                if (couleurFond%2 == 1) {
                    nodeImageTank.src = 'images/tankProfile.png';
                }
                else {
                    nodeImageTank.src = 'images/tankProfileSand.png';
                }
                nodeImageTank.alt = "Image du tank du joueur";
                nodeImageTank.style.width = "169px";
                nodeImageTank.style.backgroundColor = player["COULEUR_TANK"];

                if (element["ID_GAGNANT"] == player["ID"]) {
                    nodeWinner.appendChild(nodeUsername);
                    nodeWinner.appendChild(nodeNiveau);
                    nodeWinner.appendChild(nodeNomGenere);
                    nodeTankWinner.appendChild(nodeImageTank);
                    
                }
                else {
                    nodeLoser.appendChild(nodeUsername);
                    nodeLoser.appendChild(nodeNiveau);
                    nodeLoser.appendChild(nodeNomGenere);
                    nodeTankLoser.appendChild(nodeImageTank);
                }
                
            },this);

            gameNode.appendChild(nodeWinner);
            gameNode.appendChild(nodeTankWinner);
            gameNode.appendChild(nodeCarte);
            gameNode.appendChild(nodeLoser);
            gameNode.appendChild(nodeTankLoser);
            mainNode.appendChild(gameNode);
        }, this);
    })

    setTimeout(getLastFive, 10000);
}