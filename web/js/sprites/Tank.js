class Tank{ 
    constructor(x,y,id , color){
        this.x = x;
        this.y = y;
        this.id = id;
        this.color = color;
        this.pourcentageHP = 100;
        this.diffY = null;
        this.newPosX = null;
        this.newPosY = null;
    }
    tick(){
        if(frameCount >= results[0].length || frameCount >= results[1].length){
            return false;
        }
        
        if(this.id == 0){

            this.newPosX = parseInt(results[0][frameCount].TANK_POSITION_X)*rapportL;
            this.newPosY = parseInt(results[0][frameCount].TANK_POSITION_Y)*rapportL;
            actualTime = results[0][frameCount].TEMPS;


            this.pourcentageHP = (results[0][frameCount].TANK_HP / 200) * 100
            
            document.getElementById("hp1").innerHTML = results[0][frameCount].TANK_HP; 
            document.getElementById("hp1").style.width = this.pourcentageHP + "%" 
        }
        else if(this.id == 1){
            this.newPosX = parseInt(results[1][frameCount].TANK_POSITION_X)*rapportL;
            this.newPosY = parseInt(results[1][frameCount].TANK_POSITION_Y)*rapportL;

            actualTime = results[1][frameCount].TEMPS;
            
            this.pourcentageHP = (results[1][frameCount].TANK_HP / 200) * 100
            document.getElementById("hp2").innerHTML = results[1][frameCount].TANK_HP; 
            document.getElementById("hp2").style.width = this.pourcentageHP + "%" 
        }
        ctx.fillStyle =this.color;
        this.diffY = 350 - (this.newPosY+350);
        this.newPosY = this.newPosY + (this.diffY*2);
        ctx.fillRect(this.newPosX+350, this.newPosY+250, 20,20);

        return true;

    }
}