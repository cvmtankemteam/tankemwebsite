class Item{
    constructor(x,y,color){
        this.x = x;
        this.y = y;
        this.color = color;
        this.diffY = null;
        this.newPosX= null;
        this.newPosY = null;
    }
    tick(){
        if(itemIterator >= listeItems.length){
            return false;
        }
    
        this.newPosX = parseInt(this.x);
        this.newPosY = parseInt(this.y);
        
        this.newPosX = this.newPosX*rapportL;
        this.newPosY = this.newPosY*rapportL;
        ctx.fillStyle = this.color;
        this.diffY = 350 - (this.newPosY+350);
        this.newPosY = this.newPosY + (this.diffY*2);
        ctx.fillRect(this.newPosX+350, this.newPosY+250, 15,15);

        
        return true;

    }
}