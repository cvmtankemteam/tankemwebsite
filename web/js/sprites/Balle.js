class Balle{
    constructor(x,y,color){
        this.x = x;
        this.y = y;
        this.color = color;
        this.diffY = null; // différentiel de la distance par rapport a la moitié de la hauteur
        this.newPosX = null;
        this.newPosX = null;
    }
    tick(){
        //s'assurer que l'on itère pas à l'extérieur du tableau
        if(ballIterator >= listeBalles.length){
            return false;
        }
    
        this.newPosX = parseInt(this.x);
        this.newPosY = parseInt(this.y);
        
        this.newPosX = this.newPosX*rapportL;
        this.newPosY = this.newPosY*rapportL;
        ctx.fillStyle = this.color;
        this.diffY = 350 - (this.newPosY+350);
        this.newPosY = this.newPosY + (this.diffY*2); //reflexion de la position sur l'axe des y. les x semblent ok.
        ctx.fillRect(this.newPosX+350, this.newPosY+250, 10,10);

        return true;

    }
}