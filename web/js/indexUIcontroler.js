let currentLoginProfile;
let hallFame;
let pager = {page: 1, total:0};
$(document).ready( () => {
    
    getCurrentUser();
    getHallFame();
    
    let publicProfileTemplateNode = document.getElementById("publicProfileTemplate");
    
    let pagerCtlNode = document.getElementById("pagerCTL");
    let prevBtnNode = document.getElementById("prevBtn");
    let nextBtnNode = document.getElementById("nextBtn");
    let pageIndicatorNode = document.getElementById("pageNumber");

    pagerCtlNode.style.display = "None";

    // check if a username is pass as URL parameter 
    if(window.location.href.indexOf("username") > -1){
         let param = urlReader();
         document.getElementById("navHeaderInput").setAttribute("value", param.username)
         document.getElementById("tankemTitle").style.display = "None";
         pagerCtlNode.style.display = "flex";
         getMatchingProfileBy(param.username, 1)
     }
    
    setBackground();
    window.onresize = () =>{ setBackground() }

    let textFieldNode = document.getElementById("navHeaderInput");
    textFieldNode.onkeyup = function(event){
        
        let containerNode =  document.getElementById("globalContainer");
        let titleNode = document.getElementById("tankemTitle");

        let username = document.getElementById("navHeaderInput").value;
        
        if(username != ""){
            getMatchingProfileBy(username, pager.page);
            titleNode.style.display = "None";
            pagerCtlNode.style.display = "flex";
            pageIndicatorNode.innerHTML = pager.page + "/" + pager.total;
        }
        else{
            let allProfileContainerNode = document.getElementById("allProfileContainer");
            allProfileContainerNode.innerHTML = null;
            titleNode.style.display = "block";
            pagerCtlNode.style.display = "None";
        }
    }

    prevBtnNode.onclick = () => {
        if(pager.page == 1){
            this.style.display = "None";
        }
        else{
            this.style.display = "block";
            --pager.page
        }
    }

    nextBtnNode.onclick = () => {
        
    }
});

function setBackground() {
    let containerNode =  document.getElementById("globalContainer");
    containerNode.style.minHeight = window.innerHeight + "px";
    containerNode.style.minWidth = window.innerWidth + "px";
}

function getMatchingProfileBy(username, noPage){
    
    getCountUsername(username);
    
    $.ajax({
        url : "ajaxGetUserProfile.php",
        type : "POST",
        data: {
          "command" : "getMatchingProfileBy",
          "username" : username,
          "noPage": noPage
        }
    }).done(function(data){
        profile = JSON.parse(data);
        
        // Remove precedent profile
        let allProfileContainerNode = document.getElementById("allProfileContainer");
        allProfileContainerNode.innerHTML = null;
        
        // get profile template
        let publicProfileTemplateNode = document.getElementById("publicProfileTemplate");
        let rankImageTemplateNode = document.createElement("div");
        rankImageTemplateNode.innerHTML = document.getElementById("rankingImg").innerHTML;
        
        profile.forEach((element) => {
            // Create nodes
            let profileContainerNode = document.createElement("div");
            profileContainerNode.className = "profileContainer";
            profileContainerNode.innerHTML = publicProfileTemplateNode.innerHTML;
                
            // set values
            profileContainerNode.querySelector(".level").innerHTML = element.userInfo.NIVEAU;
            let nomCalcule = element.userInfo.USERNAME;

            if(element.userInfo.QUALIFICATIF_A != null){
                nomCalcule += " " + element.userInfo.QUALIFICATIF_A
            }
                
            if(element.userInfo.QUALIFICATIF_B != null){
                nomCalcule += " " + element.userInfo.QUALIFICATIF_B
            }
            
            // Regarde si le profile correspond à l'usager connecter
            if(currentLoginProfile != "noUser" && element.userInfo.ID == currentLoginProfile.ID){
                 nomCalcule += "  -  " + currentLoginProfile.PRENOM + " " + currentLoginProfile.NOM
             }

            profileContainerNode.querySelector(".nomCalcule").innerHTML = nomCalcule;
            profileContainerNode.querySelector(".partieJouee").innerHTML = element.stat.totalPlayed;
            profileContainerNode.querySelector(".partieGagne").innerHTML = element.stat.totalWin;
            profileContainerNode.querySelector(".carte").innerHTML = element.userInfo.FAVORITE_MAP;
            profileContainerNode.querySelector(".tank").style.background = element.userInfo.COULEUR_TANK;
            profileContainerNode.querySelector(".arme1").innerHTML = element.userInfo.ARME1;
            profileContainerNode.querySelector(".arme2").innerHTML = element.userInfo.ARME2;
            profileContainerNode.querySelector(".ratioVictoire").innerHTML = element.userInfo.WIN_RATE + " %";
            
            //related to hall of fame
            let hof = profileAgainsthallFame(element.userInfo.ID)

            profileContainerNode.querySelector(".hallFameInfo").innerHTML = hof.desc
            profileContainerNode.querySelector(".hallBadge").innerHTML = rankImageTemplateNode.querySelector("img:nth-of-type(" + hof.badgeNumber +")").outerHTML

            // append to DOM
            document.getElementById("allProfileContainer").appendChild(profileContainerNode);
        });
    });
}
function getCurrentUser(){
    
    $.ajax({
        url : "ajaxGetUserProfile.php",
        type : "POST",
        data: {
          "command" : "getCurrentUser"
        }
    }).done(function(data){
        currentLoginProfile = JSON.parse(data);
    });
}

function getHallFame(){
    $.ajax({
        url : "ajaxHallFame.php",
        type : "POST"
    }).done(function(data){
        hallFame = JSON.parse(data);
    });
}
function profileAgainsthallFame(id){
    let i;
    let found = false;
    for (i = 0; i < hallFame.length; i++){
        if(id == hallFame[i]){
            found = true;
            break;
        }
    }
    
    let badgeNumber;
    let desc;

    if(found){
        badgeNumber =  i + 1;
        desc = "Ce joueur occupe le " + badgeNumber + "e rang du Hall of Fame!"
    }
    else{
        badgeNumber = 11;
        desc = "Ce joueur ne fait pas partie du Hall of fame"
    }
    
    let rep = {badgeNumber: badgeNumber, desc: desc};
    return rep;
}
function getCountUsername(username){
    $.ajax({
        url : "ajaxGetUserProfile.php",
        type : "POST",
        data: {
          "command" : "getCountUsername",
          "username" : username
        }
    })
    .done( (data) => {
        data = JSON.parse(data)
        pager.total = Math.floor(data.NB / 5);
    });
}
function urlReader() {
    var qsParm = new Array();
    var query = window.location.search.substring(1);
    var parms = query.split('&');
    for (var i=0; i < parms.length; i++) {
        var pos = parms[i].indexOf('=');
        if (pos > 0) {
            var key = parms[i].substring(0, pos);
            var val = parms[i].substring(pos + 1);
            qsParm[key] = val;
        }
    }
    return qsParm;
}
