$(function () { 
    let $popover = $('.popover-markup>.trigger').popover({
      html: true,
      placement: 'bottom',
      // title: '<?= lang("Select attributs");?><a class="close demise");">×</a>',
      content: function () {
          return $(this).parent().find('.content').html();
      }
    });

    // open popover & inital value in form
    let attributs = [1,0,0];
    $('.popover-markup>.trigger').click(function (e) {
        e.stopPropagation();
        $(".popover-content input").each(function(i) {
            $(this).val(attributs[i]);
        });
    });
    // close popover
    $(document).click(function (e) {
        if ($(e.target).is('.demise')) {        
            $('.popover-markup>.trigger').popover('hide');
        }
    });
// store form value when popover closed
 $popover.on('hide.bs.popover', function () {
    $(".popover-content input").each(function(i) {
        attributs[i] = $(this).val();
    });
  });
    // spinner(+-btn to change value) & total to parent input 
    $(document).on('click', '.number-spinner a', function () {
        let btn = $(this),
        input = btn.closest('.number-spinner').find('input'),
        total = $('#attributs').val(),
        oldValue = input.val().trim();

    if (btn.attr('data-dir') == 'up') {

        console.log(oldValue);
        console.log(input.attr('max'));
        console.log(oldValue < input.attr('max'));

      if(Number(oldValue) < Number( input.attr('max') ) && total > 0 ){
        oldValue++;
        total--;
      }
    } else {
      if (oldValue > input.attr('min')) {
        oldValue--;
        total++;
      }
    }
    $('#attributs').val(total);
    input.val(oldValue);
  });
  $(".popover-markup>.trigger").popover('show');
});