$(document).ready( () => {
    let navHeaderBtnNode = document.getElementById("navHeaderBtn");

    // $(navHeaderBtnNode).button({
    //     icon: ".ui-icon-search"
    // });

    let searchBtnNode = document.getElementById("navHeaderBtn");
    searchBtnNode.onclick = () => {
        redirectAndSearch();
    }
    
    let textFieldNode = document.getElementById("navHeaderInput");
    textFieldNode.onkeyup = function(event){
        
        if(event.keyCode == 13){
            redirectAndSearch();
        }
    }

    getAllUsername();
});

function redirectAndSearch(){
    let username = document.getElementById("navHeaderInput").value
    
    if(username != ""){
        window.location = "index.php?username=" + username;
    }

}
function getAllUsername(){
    $.ajax({
        url : "ajaxGetUserProfile.php",
        type : "POST",
        data: {
          "command" : "getAllUsername"
        }
    }).done(function(data){
        username = JSON.parse(data);

        $("#navHeaderInput").autocomplete({
            source: username
        })
    });
}