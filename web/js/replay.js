let ctx = null;
let spriteList = [];
let playing = false;
let node = null;
let reponse; 
let tabIdPartie = [];
let connu = false;
let largeur_ctx = 700;
let hauteur_ctx = 700;
let maxVal = 5;
let minVal = -5;
let rapportLargeurJeu = maxVal - minVal;
let rapportLargeurCtx = 700-0;
let rapportL = rapportLargeurCtx/rapportLargeurJeu;
let overAllframeCount = 0;
let frameCount = 0;
let tabTimes = [];
let results = [[],[]];
let listeBalles = [];
let listeItems = [];
let maxSlidderVal = 0;
let ballIterator = 0;
let itemIterator = 0;
let mod = 0;
let idPartieSelected = null;
let actualTime = 0;

let framesInBD = 0;


window.onload = function(){

    //initialisation du slider
    node = document.getElementById("slider");
    $( function() {
        $( "#slider" ).slider();
      } );
    $( "#slider" ).slider({
        slide: function( event, ui ) {
            frameCount = ui.value;
        },
        value: 0
    });
    
    //mise a jour de la liste des 5 dernières parties
    getGames();


    //gestion d'évènement relatif à la barre de lecture
    document.getElementById("btnPlay").addEventListener('click', function(){
        if(idPartieSelected == null){
            alert("Veuillez choisir une partie.");
        }
        else{
            playing = true;
        }
             
    });
    document.getElementById("btnPause").addEventListener('click', function(){
        playing = false;
    });
    document.getElementById("slider").addEventListener('mousedown', function(){
        playing = false;
    });
    

    //gestion d'évènement relatif aux clic dans la liste d'id de partie
    document.getElementById("btn1").addEventListener('click', function(){
        let val = document.getElementById("btn1").textContent;
        idPartieSelected = val; 
    });
    document.getElementById("btn2").addEventListener('click', function(){
        let val = document.getElementById("btn2").textContent;
        idPartieSelected = val; 
    });
    document.getElementById("btn3").addEventListener('click', function(){
        let val = document.getElementById("btn3").textContent;
        idPartieSelected = val; 
    });
    document.getElementById("btn4").addEventListener('click', function(){
        let val = document.getElementById("btn4").textContent;
        idPartieSelected = val; 
    });
    document.getElementById("btn5").addEventListener('click', function(){
        let val = document.getElementById("btn5").textContent;
        idPartieSelected = val; 
    });

    //appel des fonctions ajax lorsque le bouton est cliqué.
    document.getElementById("voir").addEventListener('click', function(){
        getNomCalc(idPartieSelected);
        getDataBalles(idPartieSelected);
        getDataItems(idPartieSelected);
        getDataPartie(idPartieSelected);
    })

    ctx = document.getElementById("canvas").getContext("2d");

}

//classe un tableau d'entiers en ordre croissant
function sortNumber(a,b) {
        return a - b;
    }


function getGames(){
    $.ajax({
        url: "ajax-replay-info.php",
        type: "POST",
        data: {
            "command" : "getGames"
          }
        }).done(function(data){
            reponse = JSON.parse(data);
            for (var index = 0; index < reponse.length; index++) {
                if(tabIdPartie.length == 0 ){
                    tabIdPartie.push(reponse[index].ID_PARTIE)
                }
                else if(tabIdPartie.length > 0 ){
                    for (var i = 0; i < tabIdPartie.length; i++) {
                        if(tabIdPartie[i] == reponse[index].ID_PARTIE){
                            connu = true;
                        }    
                    }
                    if(connu == false){
                        tabIdPartie.push(reponse[index].ID_PARTIE)
                    }
                }
                connu = false;
            }
            tabIdPartie.sort(sortNumber);
            document.getElementById("btn1").innerHTML = tabIdPartie[4];
            document.getElementById("btn2").innerHTML = tabIdPartie[3];
            document.getElementById("btn3").innerHTML = tabIdPartie[2];
            document.getElementById("btn4").innerHTML = tabIdPartie[1];
            document.getElementById("btn5").innerHTML = tabIdPartie[0];
        });
    }


//retourne les infos d'une partie : position des tanks , frames , tank_hp 
function getDataPartie(idpartie){
    $.ajax({
        url: "ajax-replay-info.php",
        type: "POST",
        data: {
            "command" : "getDataPartie",
            "idpartie": idpartie
          }
    })
    .done(function (data) {
        reponse = JSON.parse(data);
        framesInBD = reponse.length/2;

        spriteList.push(new Tank(0,0,1,"red"));
        spriteList.push(new Tank(0,0,0,"blue"));
        
        for (var index = 0; index < reponse.length; index++) {
            if(reponse[index].IDENTIFIANT == 0){
                results[0].push(reponse[index]);
            }
            else if(reponse[index].IDENTIFIANT == 1){
                results[1].push(reponse[index]);
            }
            tabTimes.push(reponse[index].TEMPS);
        }
        tabTimes.sort(sortNumber);
        maxSlidderVal = tabTimes[tabTimes.length-1];
        $( "#slider" ).slider( "option", "max", maxSlidderVal );
        generalTick();
    })  
}

//retourne les infos relatives aux balles
function getDataBalles(idpartie){
    $.ajax({
        url : "ajax-replay-info.php" , 
        type: "POST" ,
        data : {
            "command" : "getDataBalles",
            "idpartie": idpartie
        }
    })
    .done(function(data){
        reponse = JSON.parse(data);
        for (var index = 0; index < reponse.length; index++) {
            listeBalles.push(reponse[index]);
        }
    })
}

//retourne les infos relatives aux items
function getDataItems(idpartie){
    $.ajax({
        url : "ajax-replay-info.php",
        type: "POST",
        data: {
            "command" : "getDataItems",
            "idpartie" : idpartie
        }
    })
    .done(function(data){
        reponse = JSON.parse(data);
        
        for (var index = 0; index < reponse.length; index++) {
            listeItems.push(reponse[index]);
            
        }
    })
}

//retourn les noms calculés des 2 joueurs
function getNomCalc(idpartie){
    $.ajax({
        url : "ajax-replay-info.php",
        type: "POST",
        data: {
            "command" : "getNomCalc",
            "idpartie" : idpartie
        }
    })
    .done(function(data){
        reponse = JSON.parse(data);
        document.getElementById("nomCalc2").innerHTML = reponse[0].NOMCALC1;
        document.getElementById("nomCalc1").innerHTML = reponse[0].NOMCALC2;
    })
}


//appelé a la fin de la fonction getDataPartie.
function generalTick() {

    //vérifier si le bouton play est cliqué et que le nombre de frame ne dépasse pas la maximum à lire.

    if(playing == true && frameCount <= maxSlidderVal && frameCount < framesInBD){
        //ralentir l'éxecution pour avoir une vitesse qui se rapproche du temps réel
        if(overAllframeCount % 5 == 0){
            $( "#slider" ).slider( "option", "value", frameCount );
            frameCount += 1;
            
            if(listeBalles.length > 0){
               if(listeBalles[ballIterator].TEMPS <= actualTime ){
                spriteList.push(new Balle(listeBalles[ballIterator].POSITION_X , listeBalles[ballIterator].POSITION_Y ,"yellow"));
                ballIterator +=1;
                } 
            }
            if(listeItems.length > 0){
                if(listeItems[itemIterator].TEMPS <= actualTime){
                    spriteList.push(new Item(listeItems[itemIterator].POSITION_X , listeItems[itemIterator].POSITION_Y, "green"));
                    itemIterator +=1;
                }
            }

            ctx.clearRect(0,0,700,700);
            for (let i = 0; i < spriteList.length; i++) {
                
                let alive = spriteList[i].tick();

                if (!alive) {
                    spriteList.splice(i, 1); 
                    i--;
                }
                
            }
        }
        overAllframeCount += 1;

    }
    //replace le curseur du slidder au départ ... n'arrive jamais malheureusement.
    if(Math.abs(frameCount) > maxSlidderVal){
        playing = false;
        frameCount = 0;
        node.lastChild.style = "left :"+frameCount +"%;";
    }
    
    window.requestAnimationFrame(generalTick);
}