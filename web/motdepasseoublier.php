<?php
	require_once("action/MotDePasseOublierAction.php");

	$action = new MotDePasseOublierAction();
	$action->execute();
	$motDePasseValide = $action->motDePasseValide;
	//require_once("reset.php");
	// if ($motDePasseValide > -1) {
	// 	header('Location: reset.php');  
	// }
	require_once("partial/header.php");
?>

<div class="conteneur">
	<div class="container">
	<h1>Mot de passe oublié ?</h1>
	<div>
			<?php 
					if ($motDePasseValide == -1) {
						?>
						<div class="alert alert-danger" role="alert">Aucun utilisateur n'utilise cette adresse courriel</div>
						<?php
					}
				?>
		</div>
		<div>
			<?php 
					if ($motDePasseValide > -1) {
						?>
						<div class="alert alert-success" role="alert">Un message de confirmation vous a été envoyé par courriel</div>
						<?php
					}
				?>
		</div>
	<form  method="post" class="form-signin">
				<div class="form-group col-md-4">
					<h3>Entrez votre adresse email</h3>
					</div>

						<div class="form-group col-md-6">
							<input id="textAreaUsername" name="emailForRecovery" class="form-control" placeholder="Adresse email" required autofocus>
						</div>
					<div class="form-group col-md-4">
						<button class="btn btn-md btn-primary" type="submit">Envoyer un courriel de vérification</button>
					</div>
	</form>
	</div>
</div>


<?php
// include  FOOTER
  require_once("partial/footer.php");