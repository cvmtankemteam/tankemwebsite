<?php
  require_once("action/ReplayAction.php");
  
  $action = new ReplayAction();
  $action->execute();

  // $info = $action->tabInfo;
  
  require_once("partial/header.php");
?>
<script src="js/sprites/Tank.js"></script>
<script src="js/sprites/Item.js"></script>
<script src="js/sprites/Balle.js"></script>
<script src="js/replay.js"></script>


<div id="title">
  <h1 class="page-header">Visionnement de Parties</h1>
  <h3>Voici les 5 dernières parties jouées : </h3>

  <input type="hidden" name="idpartie" id="partie">
  <button id="voir">Voir le replay</button>
  <div class="dropdown">
      <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      Choisir Partie
     </button>
      <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
        <a class="dropdown-item" id="btn1" href="javascript:void(0)"></a>
        <a class="dropdown-item" id="btn2" href="javascript:void(0)"></a>
        <a class="dropdown-item" id="btn3" href="javascript:void(0)"></a>
        <a class="dropdown-item" id="btn4" href="javascript:void(0)"></a>
        <a class="dropdown-item" id="btn5" href="javascript:void(0)"></a>
     </div>
    </div>
  </div>


<div id="card2" style="width: 300px;">
  <img class="card-img-top" src="images/tank2.png" alt="Card image cap">
  <div class="card-body">
    <p id="nomCalc1" class="card-text">Description (nom calculé) Joueur 1</p>
  </div>
  <div class="progress">
  <div id="hp1" class="progress-bar" role="progressbar" style="width: 100%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">  HP : 200</div>
  </div>
</div>


<div id="card1" style="width: 300px;">
  <img class="card-img-top" src="images/tank1.png" alt="Card image cap">
  <div class="card-body">
    <p id="nomCalc2" class="card-text">Description (nom calculé) Joueur 1</p>
  </div>
  <div class="progress">
  <div id="hp2" class="progress-bar" role="progressbar" style="width: 100%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">HP : 200</div>
  </div>
</div>


<div id="container">
  <button id="btnPlay" type="button" class="btn btn-default" aria-label="Left Align">
      <span class="glyphicon glyphicon-play" aria-hidden="true"></span>
  </button>
  <div id="slider"></div>
  <button id="btnPause" type="button" class="btn btn-default" aria-label="Left Align">
      <span class="glyphicon glyphicon-pause" aria-hidden="true"></span>
  </button>
</div>
<canvas id="canvas" width="700" height="700"></canvas>

<?php
// include  FOOTER
  require_once("partial/footer.php");