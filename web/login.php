<?php
	require_once("action/LoginAction.php");

	$action = new LoginAction();
	$action->execute();

	$wrongLogin = $action->wrongLogin;

	require_once("action/RegisterAction.php");	

	//$motDePasseSimilaire = $action->motDePasseSimilaire;

  require_once("partial/header.php");
?>
<!-- Local storage du nom d'usager -->
<script>
	function saveUsername() {
		localStorage["username"] = $("#textAreaUsername").val();
	}

	window.onload = function () {
		if (localStorage["username"] != null) {
			$("#textAreaUsername").val(localStorage["username"]);
		}
	}
</script>

<!-- Body -->
	<div class="conteneur">
		<div class="container">
		<!-- CHAMP MESSAGE -->
		<div>
			<?php 
				$wrongLogin;
					if ($wrongLogin) {
						?>
						<div class="alert alert-danger" role="alert"><strong>Réessayer ~</strong> Mauvais mot de passe ou nom d'utilisateur</div>
						<?php
					}
				?>
		</div>
		<div>
			<?php 
			if(isset( $_SESSION["creationSuccess"])){
					if ( $_SESSION["creationSuccess"] === true) {
						?>
						<div class="alert alert-success" role="alert"><strong>Bravo ~</strong> Vous êtes maintenant inscrit, connectez vous !</div>
						<?php
						$_SESSION["creationSuccess"] = false;
					}
				}
				?>
		</div>
		<div>
			<?php 
			if(isset($_SESSION["nomUsagerUnique"])){
				if($_SESSION["nomUsagerUnique"] === false){
					?>
					<div class="alert alert-danger" role="alert">Le nom d'usager est déjà utilisé ou est trop long, veuillez en choisir un autre (entre 1 et 20 charactères)</div>
					<?php
					$_SESSION["nomUsagerUnique"] = true;
				}
			}
				?>
		</div>
		<div>
			<?php 
			if(isset($_SESSION["samePassword"])){
				if($_SESSION['samePassword'] === false){
					?>
					<div class="alert alert-danger" role="alert">Veuillez entrez les deux même mot de passe</div>
					<?php
					$_SESSION["samePassword"]=true;
				}
			}
				?>
		</div>
		<div>
			<?php 
			if(isset($_SESSION['validEmail'])){
				if($_SESSION['validEmail'] === false){
					?>
					<div class="alert alert-danger" role="alert">Entrez un courriel valide dans ce format => John@Wick.com</div>
					<?php
					$_SESSION['validEmail']=true;
				}
			}
				?>
		</div>
		<div>
			<?php 
			if(isset($_SESSION['validName'] )){
				if($_SESSION['validName']=== false){
					?>
					<div class="alert alert-danger" role="alert">N'utiliser pas de charactère spéciaux pour votre nom/prénom ou votre nom d'usager</div>
					<?php
					$_SESSION['validName'] =true;
				}
			}
				?>
		</div>
		<!-- FIN CHAMP MESSAGE -->

			<form action="login" method="post" class="form-signin">
			<div class="form-group col-md-4">
				<h1>Connectez-vous</h1>
				</div>

					<div class="form-group col-md-6">
						<input id="textAreaUsername" name="inputEmail" class="form-control" placeholder="Nom d'usager" required autofocus>
					</div>
					<div class="form-group col-md-4">
						<input type="password" id="inputPassword" name="inputPassword" class="form-control" placeholder="Mot de passe" required>
					</div>
					<div style="form-group col-md-2" style="padding-left:10px;">
						<a href="motdepasseoublier">Mot de passe oublié ?</a>
				</div>
				<div class="form-group col-md-4">
					<button class="btn btn-md btn-primary" type="submit" onclick="saveUsername()">Se connecter</button>
				</div>
				
			</form>

		</div> <!-- /container -->

		<div class="container" style="border:3px solid black; border-radius:10px;background-color:rgba(155,255,0,0.25);">
			<form action="register" method="post">
				<div class="form-group col-md-6">
					<h2 class="form-signin-heading">Pas de compte?</h2>
				</div>
				
					<div class="form-group col-md-6">
					<label for="registerName">Nom</label>
					<input type="text" class="form-control" id="registerName" name="registerName" placeholder="Nom" required>
					</div>

					<div class="form-group col-md-6">
					<label for="registerFirstName">Prénom</label>
					<input type="text" class="form-control" id="registerFirstName" name="registerFirstName" placeholder="Prénom" required>
					</div>

				<div class="form-group">
					<div class="form-group col-md-6">
						<label for="registerEmail">Courriel</label>
						<input type="text" class="form-control" id="registerEmail" name="registerEmail" placeholder="Exemple : john@wick.com" required>
					</div>
				</div>
				<div class="form-group">
					<div class="form-group col-md-6">
						<label for="registerUsername">Nom d'usager</label>
						<input type="text" class="form-control" id="registerUsername" name="registerUsername" placeholder="Nom d'usager" required>
					</div>
				</div>

					<div class="form-group col-md-4 colorPickerContainer">
						<label for="tankColorHex">Couleur du tank</label>

						<?php require_once("partial/colorPicker.php")?>
						<input type="text" id="tankColorHex" name="tankColorHex" value="BONSOIR" readonly>

					</div>
					<div class="form-group col-md-6">
					<label for="registerPassword" >Mot de passe</label>
					<input type="password" class="form-control" id="registerPassword" name="registerPassword" required>
					</div>
					<div class="form-group col-md-6">
					<label for="confirmationPassword" >Confirmation du mot de passe</label>
					<input type="password" class="form-control" id="confirmationPassword" name="confirmationPassword" required>
					</div>

				<div class="form-group col-md-4">
					<button class="btn btn-lg btn-primary" type="submit">S'inscrire</button>
				</div>

			</form>

		</div> <!-- /container -->
</div>

<?php
// include  FOOTER
  require_once("partial/footer.php");