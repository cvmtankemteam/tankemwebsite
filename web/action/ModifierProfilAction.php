<?php
	require_once("action/CommonAction.php");
	require_once("action/dao/UserDAO.php");

	class ModifierProfilAction extends CommonAction {
		public $user = array();
		public $validationPassword =false;
		public $newPassword = false;

		public function __construct() {
			parent::__construct(parent::$VISIBILITY_MEMBER, "Modifier Profil", "<link rel='stylesheet' type='text/css' href='css/login.css' />");
		}

		protected function executeAction() {


			$connection = Connection::getConnection();
			
			$statement = $connection->prepare("SELECT * FROM TANKEM_USAGER WHERE ID = ?");
			$statement ->bindParam(1, $_SESSION["id_usager"]);
			$statement ->setFetchMode(PDO::FETCH_ASSOC);
			$statement ->execute();

			$this->user = $statement->fetch();
		}

		function saveChanges(){
			$connection = Connection::getConnection();
			
			$statementGetHash = $connection->prepare("SELECT PASSWORD FROM TANKEM_USAGER WHERE ID = ?");
			$statementGetHash ->bindParam(1, $_SESSION["id_usager"]);
			$statementGetHash ->setFetchMode(PDO::FETCH_ASSOC);
			$statementGetHash ->execute();

			$passwordHash = $statementGetHash->fetch();

			if(password_verify($_POST["currentPassword"], $passwordHash["PASSWORD"])){	
				if($_POST["newPassword"] != "")
				{
					$newpasswordHash = password_hash(($_POST["newPassword"]), PASSWORD_BCRYPT);

					UserDAO::modifyAccount($_POST["newLastName"]
										, $_POST["newFirstName"]
										, $_POST["newEmail"]
										, $_POST["newUsername"]
										, $_POST["tankColorHex"]
										, $newpasswordHash
										, $_SESSION["id_usager"]);
				}

				else
				{
					UserDAO::modifyAccount($_POST["newLastName"]
											, $_POST["newFirstName"]
											, $_POST["newEmail"]
											, $_POST["newUsername"]
											, $_POST["tankColorHex"]
											, $passwordHash["PASSWORD"]
											, $_SESSION["id_usager"]);
				}

				$this->newPassword = true;
				$this->validationPassword = true;
			}
			else{
				$this->validationPassword = false;
			}

		}

	}