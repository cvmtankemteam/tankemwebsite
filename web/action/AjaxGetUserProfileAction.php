<?php
    require_once("action/CommonAction.php");
    require_once("action/dao/UserProfileDao.php");

    class AjaxGetUserProfileAction extends CommonAction{
        
        public $result;

        public function __construct(){
            parent::__construct(CommonAction::$VISIBILITY_PUBLIC, "");
        }

        protected function executeAction(){
            if($_POST["command"] === "getUserProfile"){
                $this->result = UserProfileDao::getUserProfile();
            }
            else if($_POST["command"] === "getAllUsername"){
                $this->result = UserProfileDao::getAllUsername();
            }
            else if($_POST["command"] === "getMatchingProfileBy"){
                $this->result = UserProfileDao::getMatchingProfileBy($_POST["username"], $_POST["noPage"]);
            }
            else if($_POST["command"] === "getCurrentUser"){
                if(isset($_SESSION["id_usager"])){
                    $this->result = UserProfileDao::getCurrentUser($_SESSION["id_usager"]);
                }
                else{
                    $this->result = "noUser";
                }
            }
            else if($_POST["command"] === "getCountUsername"){
                $this->result = UserProfileDao::getCountUsername($_POST["username"]);
            }
        }
    }