<?php
	require_once("action/CommonAction.php");
	require_once("action/dao/UserDAO.php");

	class ResetAction extends CommonAction {
		public $token;
		public $mdpChanger = false;
		public $mdpChangerFail = false;

		public function __construct() {
			parent::__construct(parent::$VISIBILITY_PUBLIC, "Login", "<link rel='stylesheet' type='text/css' href='css/login.css' />");
		}

		protected function executeAction() {
			$this->token=$_GET['token'];

			if(isset($_POST["email"]) && isset($_POST["newPassword"]))
			{
				$tokenInDB = UserDAO::getToken($_POST["email"]);
				if($this->token == $tokenInDB["TOKEN"] && $tokenInDB["TOKEN"] != null)
				{
					$passwordHash = password_hash(($_POST["newPassword"]), PASSWORD_BCRYPT);
					UserDAO::resetPassword($_POST["email"], $passwordHash);
					$this->mdpChanger = true;
					UserDao::giveToken(null,$_POST["email"]);
				}
				else
				{
					$this->mdpChangerFail = true;
				}
			}
			
		}
	}
