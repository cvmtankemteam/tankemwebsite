<?php
	require_once("action/dao/Connection.php");

	class LastFiveDAO {
        public static function getLastGames() {
            $connection = Connection::getConnection();
			//select * from ( select * from emp order by sal desc ) where ROWNUM <= 5;
			$statement = $connection->prepare("SELECT * FROM (
                                                SELECT * FROM TANKEM_STATS_PARTIE ORDER BY id DESC
                                                ) WHERE ROWNUM <= 5");
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();

            $games = $statement->fetchAll();

            return $games;
        }

        public static function getUsersFromGameId($game_id) {
            $connection = Connection::getConnection();

            $statement = $connection->prepare("SELECT id, username, niveau, couleur_tank, qualificatif_a, qualificatif_b 
                                                FROM TANKEM_USAGER 
                                                WHERE ID IN (
                                                SELECT id_joueur FROM TANKEM_STATS_PARTIE_JOUEURS
                                                WHERE id_stats_partie = ?)");
            $statement->bindParam(1, $game_id);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();

            $users = $statement->fetchAll();

            return $users;
        }

    }