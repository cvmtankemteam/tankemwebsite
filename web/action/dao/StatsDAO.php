<?php
	require_once("action/dao/Connection.php");

	class StatsDAO {
        public static function getLevelName($level_id) {
            $connection = Connection::getConnection();

            $statement = $connection->prepare("SELECT nom FROM TANKEM_NIVEAU WHERE id = ?");
            $statement->bindParam(1, $level_id);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();

            $map = $statement->fetch();

            return $map["NOM"];
        }

        public static function getFavoriteLevelId($user_id) {
            $connection = Connection::getConnection();

            $statement = $connection->prepare("SELECT id_niveau , COUNT(id_niveau) AS frequence 
                                                FROM TANKEM_STATS_PARTIE 
                                                WHERE id IN (SELECT id_stats_partie FROM TANKEM_STATS_PARTIE_JOUEURS WHERE id_joueur = ?)
                                                GROUP BY id_niveau
                                                ORDER BY frequence DESC");
            $statement->bindParam(1, $user_id);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();

            $map = $statement->fetch();

            return $map["ID_NIVEAU"];

        }

        public static function getTotalMatchPlayed($user_id) {
            $connection = Connection::getConnection();

            $statement = $connection->prepare("SELECT COUNT(id_joueur) AS NOMBRE_PARTIE FROM TANKEM_STATS_PARTIE_JOUEURS WHERE id_joueur = ?");
            $statement->bindParam(1, $user_id);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();

            $match = $statement->fetch();

            return $match["NOMBRE_PARTIE"];
        }

        public static function getTotalWinGame($user_id) {
            $connection = Connection::getConnection();

            $statement = $connection->prepare("SELECT COUNT(id_gagnant) AS NOMBRE_GAGNER FROM TANKEM_STATS_PARTIE WHERE id_gagnant = ?");
            $statement->bindParam(1, $user_id);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();

            $win = $statement->fetch();

            return $win["NOMBRE_GAGNER"];
        }
    }