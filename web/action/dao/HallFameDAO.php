<?php
	require_once("action/dao/Connection.php");

	class HallFameDAO {
        public static function getUsers() {
            $connection = Connection::getConnection();
			
			$statement = $connection->prepare("SELECT * FROM (SELECT * FROM TANKEM_USAGER ORDER BY niveau DESC, win_rate DESC) WHERE ROWNUM <= 10");
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();

            $user = $statement->fetchAll();

            return $user;
        }

    }