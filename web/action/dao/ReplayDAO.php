<?php
	require_once("action/dao/Connection.php");

	class ReplayDAO {

        public static function getInfoPartieReplay($idpartie){
			$connection = Connection::getConnection();
			$id_partie = $idpartie;
            $statement = $connection->prepare("SELECT * FROM TANKEM_PARTIE WHERE id_partie = ? ORDER BY temps asc");
            $statement->bindParam(1,$id_partie);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            $statement->execute();
            
            $info = null;
            $info = $statement->fetchAll();
            return $info;
		}
		
		public static function getGames(){
			$connection = Connection::getConnection();
			$statement = $connection->prepare("SELECT id_partie FROM TANKEM_PARTIE");
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();
			$info = null;
			$info = $statement->fetchALL();
			return $info;
		}

		public static function getInfoBallesReplay($idpartie){
			$connection = Connection::getConnection();
			$id_partie = $idpartie;
            $statement = $connection->prepare("SELECT * FROM TANKEM_BALLES_REPLAY WHERE id_partie = ? ORDER BY temps asc");
            $statement->bindParam(1,$id_partie);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            $statement->execute();
            
            $info = null;
            $info = $statement->fetchAll();
            return $info;
		}

		public static function getInfoItemsReplay($idpartie){
			$connection = Connection::getConnection();
			$id_partie = $idpartie;
            $statement = $connection->prepare("SELECT * FROM TANKEM_ITEMS_REPLAY WHERE id_partie = ? ORDER BY temps asc");
            $statement->bindParam(1,$id_partie);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            $statement->execute();
            
            $info = null;
            $info = $statement->fetchAll();
            return $info;

		}
		public static function getNomCalc($idpartie){
			$connection = Connection::getConnection();
			$id_partie = $idpartie;
            $statement = $connection->prepare("SELECT * FROM TANKEM_INFO_PARTIE WHERE id_partie = ?");
            $statement->bindParam(1,$id_partie);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            $statement->execute();
            
            $info = null;
            $info = $statement->fetchAll();
            return $info;
		}

		// public static function authenticate($username, $password) {
		// 	$connection = Connection::getConnection();
			
		// 	$statement = $connection->prepare("SELECT * FROM USERS WHERE USERNAME = ?");
		// 	$statement->bindParam(1, $username);
		// 	$statement->setFetchMode(PDO::FETCH_ASSOC);
		// 	$statement->execute();

		// 	$user = null;

		// 	if ($row = $statement->fetch()) {

		// 		if (password_verify($password, $row["PASSWORD"])) {
		// 			$user = $row;
		// 		}
		// 	}

		// 	return $user;
		// }

		// public static function updateProfile($user) {
		// 	$connection = Connection::getConnection();

		// 	$statement = $connection->prepare("UPDATE USERS SET first_name = ? WHERE id = ?");
		// 	$statement->bindParam(1, $user["FIRST_NAME"]);
		// 	$statement->bindParam(2, $user["ID"]);
		// 	$statement->execute();
		// }
	}