<?php
	require_once("action/dao/Connection.php");

	class UserDAO {

		public static function authenticate($username, $password) {
			$connection = Connection::getConnection();
			
			$statement = $connection->prepare("SELECT * FROM USERS WHERE USERNAME = ?");
			$statement->bindParam(1, $username);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();

			$user = null;

			if ($row = $statement->fetch()) {

				if (password_verify($password, $row["PASSWORD"])) {
					$user = $row;
				}
			}

			return $user;
		}

		public static function updateProfile($user) {
			$connection = Connection::getConnection();

			$statement = $connection->prepare("UPDATE USERS SET first_name = ? WHERE id = ?");
			$statement->bindParam(1, $user["FIRST_NAME"]);
			$statement->bindParam(2, $user["ID"]);
			$statement->execute();
		}
	}