<?php
	require_once("action/dao/Connection.php");

	class UserDAO {

		public static function authenticate($username, $password) {
			$visibility = 0;
			$connection = Connection::getConnection();
			
			$statementGetHash = $connection->prepare("SELECT PASSWORD FROM TANKEM_USAGER WHERE USERNAME = ?");
			$statementGetHash ->bindParam(1, $username);
			$statementGetHash ->setFetchMode(PDO::FETCH_ASSOC);
			$statementGetHash ->execute();

			$passwordHash = $statementGetHash->fetch();

			if(password_verify($password, $passwordHash["PASSWORD"])){			
				$statement = $connection->prepare("SELECT ID FROM TANKEM_USAGER WHERE USERNAME = ? AND PASSWORD = ?");
				$statement->bindParam(1, $username);
				$statement->bindParam(2, $passwordHash["PASSWORD"]);
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();

				$user = $statement->fetch();


				if ($user) {
					$_SESSION["id_usager"] = $user["ID"];
					$visibility = 1;
				}
			}

			return $visibility;
		}

		public static function createAccount($name, $firstName, $email, $username, $tankColorHex, $password) {
			$visibility = 0;
			$connection = Connection::getConnection();
			try{
				$statement = $connection->prepare("INSERT INTO TANKEM_USAGER (nom, prenom, courriel, username, couleur_tank, password) VALUES (?,?,?,?,?,?)");
				$statement->bindParam(1, $name);
				$statement->bindParam(2, $firstName);
				$statement->bindParam(3, $email);
				$statement->bindParam(4, $username);
				$statement->bindParam(5, $tankColorHex);
				$statement->bindParam(6, $password);
				$statement->execute();
				$_SESSION["creationSuccess"] = true;
			}
			catch(Exception $e){
				$_SESSION["nomUsagerUnique"] = false;
				$_SESSION["creationSuccess"] = false;
			}

			

		}	


		public static function modifyAccount($name, $firstName, $email, $username, $tankColorHex, $password, $id) {
			$connection = Connection::getConnection();
			
			$statement = $connection->prepare("UPDATE TANKEM_USAGER SET nom = ?, prenom = ?, courriel = ?, username = ?, couleur_tank = ?, password = ? WHERE ID=?");
			$statement->bindParam(1, $name);
			$statement->bindParam(2, $firstName);
			$statement->bindParam(3, $email);
			$statement->bindParam(4, $username);
			$statement->bindParam(5, $tankColorHex);
			$statement->bindParam(6, $password);
			$statement->bindParam(7, $id);
			$statement->execute();

		}	

		public static function emailPresent($email){	
			$connection = Connection::getConnection();		
			$statement = $connection->prepare("SELECT ID FROM TANKEM_USAGER WHERE COURRIEL = ?");
			$statement->bindParam(1, $email);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();

			$user = $statement->fetch();
			if($user == null)
			{
				$user = -1;
			}
			return $user;

		}

		public static function giveToken($token, $email){
			$connection = Connection::getConnection();
			$statement = $connection->prepare("UPDATE TANKEM_USAGER SET token=? WHERE COURRIEL = ?");
			$statement->bindParam(1, $token);
			$statement->bindParam(2, $email);
			$statement->execute();
		}

		public static function getToken($email){
			$connection = Connection::getConnection();
			$statement = $connection->prepare("SELECT TOKEN FROM TANKEM_USAGER WHERE COURRIEL = ?");
			$statement->bindParam(1, $email);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();

			$token = $statement->fetch();

			return $token;

		}

		public static function resetPassword($email, $password) {
			$connection = Connection::getConnection();
			
			$statement = $connection->prepare("UPDATE TANKEM_USAGER SET PASSWORD = ? WHERE COURRIEL=?");
			$statement->bindParam(1, $password);
			$statement->bindParam(2, $email);
			$statement->execute();

		}	
		
		

	}