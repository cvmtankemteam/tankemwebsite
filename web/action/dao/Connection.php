<?php

	class Connection {
		
		private static $connection = null;
		
		public static function getConnection() {
			if (Connection::$connection == null) {
				try{
				Connection::$connection = new PDO(DB_NAME, DB_USER, DB_PASS);
				Connection::$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				Connection::$connection->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
				}
				catch(Exception $e){
					header("location:404");
				}
			}

			return Connection::$connection;
		}
	}