<?php
    require_once("action/dao/Connection.php");
    require_once("action/dao/StatsDAO.php");

    class UserProfileDao{

        public static function getUserProfile(){
            $connection = Connection::getConnection();

            $statement = $connection->prepare("SELECT ID, USERNAME, NIVEAU, COULEUR_TANK, QUALIFICATIF_A, QUALIFICATIF_B FROM TANKEM_USAGER");
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            $statement->execute();

            $profiles = $statement->fetchAll();
            $username = array();
            $allProfile = array();

            foreach($profiles as $profile){
                $username[] = $profile["USERNAME"];
                $stat = array(
                    "bestMap" => StatsDAO::getFavoriteLevel($profile["ID"]),
                    "totalWin" => StatsDAO::getTotalWinGame($profile["ID"]),
                    "totalPlayed" => StatsDAO::getTotalMatchPlayed($profile["ID"])
                );
                $allProfile[] = array(
                    "userInfo" => $profile,
                    "stat" => $stat 
                );
            }

            $result = array(
                "allUsername" => $username,
                "allProfile" => $allProfile
            );

            return $result;
        }

        public static function getAllUsername(){
            $connection = Connection::getConnection();
            
            $statement = $connection->prepare("SELECT USERNAME FROM TANKEM_USAGER");
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            $statement->execute();

            $allUsername = $statement->fetchAll();
            $result = array();
            foreach($allUsername as $name){
                $result[] = $name["USERNAME"];
            }
            return $result;
        }

        public static function getMatchingProfileBy($username, $noPage){
            $connection = Connection::getConnection();
            
            $username .= "%";
            $upperPage = $noPage * 5;
            $lowerPage = $upperPage - 4; // - 4 parce que les ranger commence a 1 dans SQL
            
            // Compter le nombre de profile correscpondant
            $nbProfileStatement = $connection->prepare("SELECT COUNT(ID) FROM TANKEM_USAGER WHERE USERNAME LIKE ?");
            $nbProfileStatement->bindParam(1, $username);
            $nbProfileStatement->setFetchMode(PDO::FETCH_ASSOC);
            $nbProfileStatement->execute();
            
            $nbProfile = $nbProfileStatement->fetch();

            // Obtenir les profiles par page de 5 elements
            $statement = $connection->prepare("SELECT * FROM (SELECT ID, USERNAME, NIVEAU, COULEUR_TANK, QUALIFICATIF_A, QUALIFICATIF_B, WIN_RATE, FAVORITE_MAP, ARME1, ARME2 FROM TANKEM_USAGER WHERE USERNAME LIKE ? ORDER BY USERNAME ASC) WHERE ROWNUM BETWEEN ? AND ?");
            $statement->bindParam(1, $username);
            $statement->bindParam(2, $lowerPage);
            $statement->bindParam(3, $upperPage);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            $statement->execute();
            
            $profiles = $statement->fetchAll();
            $username = array();
            $allProfile = array();
            
            foreach($profiles as $profile){
                $stat = array(
                    "totalWin" => StatsDAO::getTotalWinGame($profile["ID"]),
                    "totalPlayed" => StatsDAO::getTotalMatchPlayed($profile["ID"])
                );
                
                $allProfile[] = array(
                    "userInfo" => $profile,
                    "stat" => $stat
                    );
            }

            return $allProfile;
        }

        public static function getCountUsername($username){
            $connection = Connection::getConnection();
            
            $username .= "%";
            
            // Compter le nombre de profile correscpondant
            $nbProfileStatement = $connection->prepare("SELECT COUNT(ID) AS NB FROM TANKEM_USAGER WHERE USERNAME LIKE ?");
            $nbProfileStatement->bindParam(1, $username);
            $nbProfileStatement->setFetchMode(PDO::FETCH_ASSOC);
            $nbProfileStatement->execute();
            
            $nbProfile = $nbProfileStatement->fetch();
            return $nbProfile;
        }

        public static function getCurrentUser($id){
            $connection = Connection::getConnection();
            
            $statement = $connection->prepare("SELECT ID, PRENOM, NOM FROM TANKEM_USAGER WHERE ID = ?");
            $statement->bindParam(1, $id);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            $statement->execute();
            
            $current = $statement->fetch();
            return $current;
        }
    }