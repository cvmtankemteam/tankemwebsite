<?php
	require_once("action/dao/Connection.php");

	class AttributDAO {
        public static function getUser($user_id) {
            $connection = Connection::getConnection();
			
			$statement = $connection->prepare("SELECT * FROM TANKEM_USAGER WHERE ID = ?");
			$statement->bindParam(1, $user_id);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();

            $user = $statement->fetch();

            return $user;
        }

        public static function addPoint( $remaining_point , $life_point , $force_point , $dex_point , $agil_point , $userid ) {
            $connection = Connection::getConnection();

            $statement = $connection->prepare("UPDATE TANKEM_USAGER SET POINT_ATTRIBUT = ? , VIE = ? , FORCE = ? , DEXTERITE = ? , AGILITE = ? WHERE ID = ?");
			$statement->bindParam(1, $remaining_point);
            $statement->bindParam(2, $life_point);
            $statement->bindParam(3, $force_point);
            $statement->bindParam(4, $dex_point);
            $statement->bindParam(5, $agil_point);
            $statement->bindParam(6, $userid);
			$statement->execute();
        }

        public static function getBalance() {
            $connection = Connection::getConnection();

            $statement = $connection->prepare("SELECT nom_variable, valeur_variable FROM TANKEM_DONNEE_FLOAT");
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();

            $balance = $statement->fetchAll();

            return $balance;
        }

        public static function setNomQualificatif($a , $b , $user_id) {
            $connection = Connection::getConnection();

            $statement = $connection->prepare("UPDATE TANKEM_USAGER SET qualificatif_a = ? , qualificatif_b = ? WHERE id = ?");
			$statement->bindParam(1, $a);
            $statement->bindParam(2, $b);
            $statement->bindParam(3, $user_id);
			$statement->execute();
        }
    }