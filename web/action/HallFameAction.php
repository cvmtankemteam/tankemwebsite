<?php
	require_once("action/CommonAction.php");
    require_once("action/dao/HallFameDAO.php");
    require_once("action/dao/StatsDAO.php");

	class HallFameAction extends CommonAction {
        public $listTopUsers = array();
        public $logoArray = array();

		public function __construct() {
			parent::__construct(parent::$VISIBILITY_PUBLIC, "Hall of Fame" , "<link rel='stylesheet' type='text/css' href='css/lastfive-hallfame-attribut.css' />");
            $this->logoArray[] = "<img src='images/hallfameBadges/sergentMajorOfArmy.png' alt='Image Ranking'>";
            $this->logoArray[] = "<img src='images/hallfameBadges/sergentCommandant.png' alt='Image Ranking'>";
            $this->logoArray[] = "<img src='images/hallfameBadges/sergentMajor.png' alt='Image Ranking'>";
            $this->logoArray[] = "<img src='images/hallfameBadges/premierSergent.png' alt='Image Ranking'>";
            $this->logoArray[] = "<img src='images/hallfameBadges/masterSergent.png' alt='Image Ranking'>";
            $this->logoArray[] = "<img src='images/hallfameBadges/sergentFirstClass.png' alt='Image Ranking'>";
            $this->logoArray[] = "<img src='images/hallfameBadges/stafSergent.png' alt='Image Ranking'>";
            $this->logoArray[] = "<img src='images/hallfameBadges/sergent.png' alt='Image Ranking'>";
            $this->logoArray[] = "<img src='images/hallfameBadges/corporal.png' alt='Image Ranking'>";
            $this->logoArray[] = "<img src='images/hallfameBadges/firstClass.png' alt='Image Ranking'>";
		}

		protected function executeAction() {
      		$listUsers = HallFameDAO::getUsers();
            $updatedListUsers = array();

            foreach ($listUsers as $user) {
                $user["GAME_PLAYED"] = StatsDAO::getTotalMatchPlayed($user["ID"]);
                $updatedListUsers[] = $user;
            }

            $this->listTopUsers = $updatedListUsers;
		}
	}
