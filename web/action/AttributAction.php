<?php
	require_once("action/CommonAction.php");
    require_once("action/dao/AttributDAO.php");

	class AttributAction extends CommonAction {
        public $point_attribut;
        public $point_vie;
        public $point_force;
        public $point_dex;
        public $point_agil;

        public $stats_vie;
        public $stats_force;
        public $stats_dex;
        public $stats_agil;

        public $error;

		public function __construct() {
			parent::__construct(parent::$VISIBILITY_MEMBER, "Attribut" , "<link rel='stylesheet' type='text/css' href='css/lastfive-hallfame-attribut.css' />");
            $this->error = false;
		}

		protected function executeAction() {
      		$user = AttributDAO::getUser( $_SESSION["id_usager"] );

            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $total_original = $user["POINT_ATTRIBUT"] + $user["VIE"] + $user["FORCE"] + $user["DEXTERITE"] + $user["AGILITE"];
                $total_update = (int) $_POST["attributs"] + (int) $_POST["vie"] + (int) $_POST["force"] + (int) $_POST["dex"] + (int) $_POST["agil"];

                $maximum = true;
                if (    (int) $_POST["vie"] > 30
                    || (int) $_POST["force"] > 30
                    || (int) $_POST["dex"] > 30
                    || (int) $_POST["agil"] > 30) {
                        $maximum = false;
                    }

                if ($total_original == $total_update
                    && $maximum) {
                    AttributDAO::addPoint( (int) $_POST["attributs"] , (int) $_POST["vie"] , (int) $_POST["force"] , (int) $_POST["dex"] , (int) $_POST["agil"] , $_SESSION["id_usager"] );
                }
                else {
                    $this->error = true;
                }

                // Nom qualificatif du joueur
                $qualificatif = array();
                $stats_table = array( "VIE" => (int) $_POST["vie"] , "FORCE" => (int) $_POST["force"] , "DEXTERITE" => (int) $_POST["dex"] , "AGILITE" => (int) $_POST["agil"]);
                arsort($stats_table);
                $counter = 0;
                reset($stats_table);
                if ($maximum) {
                    while ($stat_value = current($stats_table) && $counter < 2) {
                    $temp = current($stats_table);
                    if ( key($stats_table) == "VIE" ) {
                        if ( $temp >= 10 ) {
                            if ($counter == 0) {
                                $qualificatif[] = "L'immortel";
                            }
                            else {
                                $qualificatif[] = "immortel";
                            }
                        }
                        elseif ( $temp >= 5 ) {
                            if ($counter == 0) {
                                $qualificatif[] = "Le pétulant";
                            }
                            else {
                                $qualificatif[] = "pétulant";
                            }
                        }
                        elseif ( $temp >= 1 ) {
                            if ($counter == 0) {
                                $qualificatif[] = "Le fougueux";
                            }
                            else {
                                $qualificatif[] = "fougueux";
                            }
                        }
                    }
                    elseif (key($stats_table) == "FORCE") {
                        if ( $temp >= 10 ) {
                            if ($counter == 0) {
                                $qualificatif[] = "Le tout puissant";
                            }
                            else {
                                $qualificatif[] = "tout puissant";
                            }
                        }
                        elseif ( $temp >= 5 ) {
                            if ($counter == 0) {
                                $qualificatif[] = "Le hulk";
                            }
                            else {
                                $qualificatif[] = "brutal";
                            }
                        }
                        elseif ( $temp >= 1 ) {
                            if ($counter == 0) {
                                $qualificatif[] = "Le crossfiteur";
                            }
                            else {
                                $qualificatif[] = "qui fait du crossfit";
                            }
                        }
                    }
                    elseif (key($stats_table) == "DEXTERITE") {
                        if ( $temp >= 10 ) {
                            if ($counter == 0) {
                                $qualificatif[] = "Le chirurgien";
                            }
                            else {
                                $qualificatif[] = "chirurgien";
                            }
                        }
                        elseif ( $temp >= 5 ) {
                            if ($counter == 0) {
                                $qualificatif[] = "L'habile'";
                            }
                            else {
                                $qualificatif[] = "habile";
                            }
                        }
                        elseif ( $temp >= 1 ) {
                            if ($counter == 0) {
                                $qualificatif[] = "Le précis";
                            }
                            else {
                                $qualificatif[] = "précis";
                            }
                        }
                    }
                    elseif (key($stats_table) == "AGILITE") {
                        if ( $temp >= 10 ) {
                            if ($counter == 0) {
                                $qualificatif[] = "Le foudroyant";
                            }
                            else {
                                $qualificatif[] = "foudroyant";
                            }
                        }
                        elseif ( $temp >= 5 ) {
                            if ($counter == 0) {
                                $qualificatif[] = "Le lynx";
                            }
                            else {
                                $qualificatif[] = "lynx";
                            }
                        }
                        elseif ( $temp >= 1 ) {
                            if ($counter == 0) {
                                $qualificatif[] = "Le prompt";
                            }
                            else {
                                $qualificatif[] = "prompt";
                            }
                        }
                    }

                    next($stats_table);
                    $counter++;
                }

                if ( (int) $_POST["vie"] == 30 &&
                    (int) $_POST["force"] == 30 &&
                    (int) $_POST["dex"] == 30 &&
                     (int) $_POST["agil"] == 30) {
                        $qualificatif = array();
                        $qualificatif[] = "Le dominant";
                }
                elseif( sizeof($qualificatif) == 0 ) {
                    $qualificatif[] = "Aucun";
                    $qualificatif[] = "Aucun";
                }
                elseif( sizeof($qualificatif) == 1 ) {
                    $qualificatif[] = "";
                }
                }

                AttributDAO::setNomQualificatif( $qualificatif[0] , $qualificatif[1] , $_SESSION["id_usager"] );
            }
            // Attribut du tank du joueur
            $user = AttributDAO::getUser( $_SESSION["id_usager"] );
            $this->point_attribut = $user["POINT_ATTRIBUT"];
            $this->point_vie = $user["VIE"];
            $this->point_force = $user["FORCE"];
            $this->point_dex = $user["DEXTERITE"];
            $this->point_agil = $user["AGILITE"];

            // Statistiques du tank
            $balance = AttributDAO::getBalance();
            $this->stats_vie = $balance[2]["VALEUR_VARIABLE"] + ($balance[2]["VALEUR_VARIABLE"] * 0.1) * $this->point_vie;
            $this->stats_force = 1 + 1 * 0.1 * $this->point_force;
            $this->stats_dex = 0.1 * $this->point_dex;
            $this->stats_agil = $balance[0]["VALEUR_VARIABLE"] + ($balance[0]["VALEUR_VARIABLE"] * 0.05) * $this->point_dex;
		}
	}