<?php
	require_once("action/CommonAction.php");
    require_once("action/dao/LastFiveDAO.php");
	require_once("action/dao/StatsDAO.php");

	class AjaxLastFiveAction extends CommonAction {
        public $result;
		
		public function __construct() {
			parent::__construct(parent::$VISIBILITY_PUBLIC,"");
			$this->result = array();
		}

		protected function executeAction() {
			$lastgames = LastFiveDAO::getLastGames();

            foreach ($lastgames as $game) {
                $game["PLAYERS"] = LastFiveDAO::getUsersFromGameId($game["ID"]);
				$game["NOM_NIVEAU"] = StatsDAO::getLevelName($game["ID_NIVEAU"]);
                $this->result[] = $game;
            }
		}
	}