<?php
    require_once("action/CommonAction.php");
    require_once("action/dao/ReplayDAO.php");

	class AjaxReplayAction extends CommonAction {
		public $infoPartie;
		public $tabInfo = [];

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC,"");
		}

		protected function executeAction() {
			if($_POST["command"] === "getGames"){
				$this->infoPartie = ReplayDAO::getGames();
				foreach($this->infoPartie as $row){
					$this->tabInfo[] = $row;
				}
			}
			if($_POST["command"] === "getDataPartie"){
				$this->infoPartie =  ReplayDAO::getInfoPartieReplay($_POST["idpartie"]);
				foreach ($this->infoPartie as $row) {
					$this->tabInfo[] = $row;
				}	
			 }
			 if($_POST["command"] === "getDataBalles"){
				 $this->infoPartie = ReplayDAO::getInfoBallesReplay($_POST["idpartie"]);
				 foreach ($this->infoPartie as $row) {
					 $this->tabInfo[] = $row;
				 }
			 }
			 if($_POST["command"] === "getDataItems"){
				$this->infoPartie = ReplayDAO::getInfoItemsReplay($_POST["idpartie"]);
				foreach ($this->infoPartie as $row) {
					$this->tabInfo[] = $row;
				}
			 }
			 if($_POST["command"] == "getNomCalc"){
				$this->infoPartie = ReplayDAO::getNomCalc($_POST["idpartie"]);
				foreach ($this->infoPartie as $row) {
					$this->tabInfo[] = $row;
				}
			 }
        }
}