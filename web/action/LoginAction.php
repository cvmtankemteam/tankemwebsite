<?php
	require_once("action/CommonAction.php");
	require_once("action/dao/UserDAO.php");

	class LoginAction extends CommonAction {
		public $wrongLogin = false;

		public function __construct() {
			parent::__construct(parent::$VISIBILITY_PUBLIC, "Login", "<link rel='stylesheet' type='text/css' href='css/login.css' />");
		}

		protected function executeAction() {

			if ($_SESSION["visibility"] > 0) {
				header("location:attribut");
				exit;
			}

			if (isset($_POST["inputEmail"])) {
				$visibility = UserDAO::authenticate($_POST["inputEmail"], $_POST["inputPassword"]);

				if ($visibility > 0) {
					$_SESSION["visibility"] = $visibility;
					// $_SESSION["username"] = $_POST["username"];
						
					header("location:attribut");
					exit;
				}
				else {
					$this->wrongLogin = true;
				}
			}
		}
	}
