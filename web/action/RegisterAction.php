<?php
	require_once("action/CommonAction.php");
	require_once("action/dao/UserDAO.php");

	class RegisterAction extends CommonAction {
		public $goodRegister=false;


		public function __construct() {
			parent::__construct(parent::$VISIBILITY_PUBLIC, "Register");
		}

		protected function executeAction() {
			$_SESSION['samePassword'] =true;
			$_SESSION['validEmail'] =true;
			$_SESSION['validName'] =true;
			// if(preg_match('/^[\p{L}\p{N}]+$/u', $_POST["registerName"]) || preg_match('/^[\p{L}\p{N}]+$/u', $_POST["registerFirstName"]) || preg_match('/^[\p{L}\p{N}]+$/u', $_POST["registerUsername"]))
			// {
			//    $_SESSION['validName'] = false;
			// }
			if (!filter_var($_POST["registerEmail"], FILTER_VALIDATE_EMAIL)){
				$_SESSION['validEmail'] =false;
				$_SESSION["creationSuccess"]=false;
			}
			else if($_POST["confirmationPassword"] === $_POST["registerPassword"]){
				if (isset($_POST["registerPassword"])) {

					$passwordHash = password_hash(($_POST["registerPassword"]), PASSWORD_BCRYPT);

					UserDAO::createAccount($_POST["registerName"]
											, $_POST["registerFirstName"]
											, $_POST["registerEmail"]
											, $_POST["registerUsername"]
											, $_POST["tankColorHex"]
											, $passwordHash);


				}
			}
			else
			{
				$_SESSION['samePassword'] = false;
				$_SESSION["creationSuccess"]=false;
			}
			header("location:login");
			exit;
		}
	}
