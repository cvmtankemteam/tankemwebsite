<?php

	require_once("action/CommonAction.php");
	// Démarrer une nouvelle session, ou recharger celle existante


class LogoutAction extends CommonAction{

	public function __construct(){
		parent::__construct(parent::$VISIBILITY_MEMBER, "Logout");
	}

	function executeAction() {
		$_SESSION["visibility"] = 0;
		
		session_unset();
		session_destroy();

		header("location:login");
		exit;
	}
}