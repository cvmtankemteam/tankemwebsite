<?php
	session_start();

	require_once("action/constants.php");
	
	abstract class CommonAction {
		public static $VISIBILITY_PUBLIC = 0;
		public static $VISIBILITY_MEMBER = 1;
		public static $VISIBILITY_MODERATOR = 2;
		public static $VISIBILITY_ADMIN = 3;

		private $pageVisibility;
		private $pageTitle;
		private $pageCssFile;

		public function __construct($pageVisibility, $pageTitle, $pageCssFile = null) {
			$this->pageVisibility = $pageVisibility;
			$this->pageTitle = $pageTitle;
			$this->pageCssFile = $pageCssFile;
		}

		public function execute() {			
			if (!empty($_GET["action"]) && $_GET["action"] == "logout") {
				session_unset();
				session_destroy();
				session_start();
			}
			
			if (empty($_SESSION["visibility"])) {
				$_SESSION["visibility"] = CommonAction::$VISIBILITY_PUBLIC;
			}

			if ($_SESSION["visibility"] < $this->pageVisibility) {
				header("location:login");
				exit;
			}

			$this->executeAction();
		}

		// Template method
		abstract protected function executeAction();

		public function isLoggedIn() {
			return $_SESSION["visibility"] > CommonAction::$VISIBILITY_PUBLIC;
		}

		public function getUsername() {
			// Condition ternaire
			return $this->isLoggedIn() ? $_SESSION["username"] : "invité";
		}

		public function getPageTitle() {
			echo $this->pageTitle;
		}

		public function getPageCssFile() {
			if( isset($this->pageCssFile) && strpos($this->pageCssFile, "stylesheet")) {
				echo $this->pageCssFile;
			}
		}

		function generateRandomString($length = 40) {
			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
			$charactersLength = strlen($characters);
			$randomString = '';
			for ($i = 0; $i < $length; $i++) {
				$randomString .= $characters[rand(0, $charactersLength - 1)];
			}
			return $randomString;
		}
		
		public function mailresetlink($to,$token){
			$subject = "Forgot Password on TANKEM";
			$uri = 'http://localhost/tankemwebsite/web' ;
			$message = '
			<html>
			<head>
			<title>Forgot Password For TANKEM</title>
			</head>
			<body>
			<img src="http://i0.kym-cdn.com/photos/images/original/000/666/559/acf.gif" alt="someone is trying to hack you">
			<p>Cliquer sur ce lien pour reinitialiser votre mot de passe<br>
			si vous etes bien la personne en ayant fait la demande, cliquer sur ce lien : <br>
			<a href="'.$uri.'/reset?token='.$token["TOKEN"].'">Reset Password</a></p>
			
			</body>
			</html>
			';
			$headers = "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
			$headers .= 'From: God<tank@em.com>' . "\r\n";
			$headers .= 'Cc: tank@em.com' . "\r\n";
			mail($to,$subject,$message,$headers);

		}
	}