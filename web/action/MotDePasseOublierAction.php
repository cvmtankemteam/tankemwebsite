<?php
	require_once("action/CommonAction.php");
	require_once("action/dao/UserDAO.php");

	class MotDePasseOublierAction extends CommonAction {

		public $motDePasseValide = false;

		public function __construct() {
			parent::__construct(parent::$VISIBILITY_PUBLIC, "Login", "<link rel='stylesheet' type='text/css' href='css/login.css' />");
		}

		protected function executeAction() {

			if (isset($_POST["emailForRecovery"])) {
				$this->motDePasseValide = UserDAO::emailPresent($_POST["emailForRecovery"]);

				if($this->motDePasseValide > -1){
					UserDAO::giveToken(CommonAction::generateRandomString(), $_POST["emailForRecovery"]);
					CommonAction::mailresetlink($_POST["emailForRecovery"], UserDAO::getToken($_POST["emailForRecovery"]));
						
				}

			}
			
		}

}
