<?php
    require_once("action/AjaxReplayAction.php");

    $action = new AjaxReplayAction();
	$action->execute();
    echo json_encode($action->tabInfo);
